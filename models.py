from django.db import models
from django.contrib.auth.models import AbstractUser
import uuid


class User(AbstractUser):
    class Role(models.TextChoices):
        ADMIN = "ADMIN", 'Admin'

    USERNAME_FIELD = 'email'
    base_role = Role.ADMIN
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    email = models.EmailField('email address', unique=True)
    role = models.CharField(max_length=50, choices=Role.choices)
    REQUIRED_FIELDS = ['username']

    def save(self, *args, **kwargs):
        self.role = self.base_role
        return super().save(*args, **kwargs)
